//
//  MasterViewController.m
//  TestAssignment
//
//  Created by Yury Pogrebnyak on 17.01.15.
//  Copyright (c) 2015 room8studio. All rights reserved.
//

#import "PeopleViewController.h"
#import "FriendsViewController.h"
#import "APIHelper.h"
#import "Person.h"
#import "PersonTableViewCell.h"

@interface PeopleViewController ()

@end

@implementation PeopleViewController

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(loadPeopleFromServer) forControlEvents:UIControlEventValueChanged];
    
    // always fetch items from Core Data first
    [self loadPeopleFromCoreData];
    
    // attempt to update items from server
    [self.refreshControl beginRefreshing];
    [self loadPeopleFromServer];
}

#pragma mark - Loading data

- (void) loadPeopleFromCoreData {
    self.people = [[APIHelper sharedInstance] fetchAllPeople];
    [self.tableView reloadData];
}

- (void) loadPeopleFromServer {
    [[APIHelper sharedInstance] getAllPeopleWithSuccess:^(NSArray *people) {
        [self.refreshControl endRefreshing];
        self.people = people;
        [self.tableView reloadData];
    } andFailure:^(NSError *error) {
        [self.refreshControl endRefreshing];
        [self loadPeopleFromCoreData];
    }];
}

#pragma mark - Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowFriendsSegue"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSSet* friends = ((Person*)self.people[indexPath.row]).friends;
        FriendsViewController* destinationViewController = (FriendsViewController*) segue.destinationViewController;
        destinationViewController.friends = [friends sortedArrayUsingDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"friendId" ascending:YES]]];
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.people.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PersonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PersonCell" forIndexPath:indexPath];
    Person* person = self.people[indexPath.row];
    cell.guidLabel.text = person.guid;
    cell.companyLabel.text = person.company;
    cell.emailLabel.text = person.email;
    cell.addressLabel.text = person.address;
    cell.phoneLabel.text = person.phone;
    cell.balanceLabel.text = person.balance;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 156;
}

@end
