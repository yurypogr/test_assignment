//
//  MasterViewController.h
//  TestAssignment
//
//  Created by Yury Pogrebnyak on 17.01.15.
//  Copyright (c) 2015 room8studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface PeopleViewController : UITableViewController

@property (nonatomic, strong) NSArray* people;

@end

