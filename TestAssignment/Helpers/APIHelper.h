//
//  APIHelper.h
//  TestAssignment
//
//  Created by Yury Pogrebnyak on 17.01.15.
//  Copyright (c) 2015 room8studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData.h>
#import <RestKit.h>

@interface APIHelper : NSObject

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;

@property (strong, nonatomic) RKObjectManager *objectManager;
@property (strong, nonatomic) RKManagedObjectStore *objectStore;

//------------------------------------------------------------------------
#pragma mark - Singleton

+ (APIHelper*)sharedInstance;

//------------------------------------------------------------------------
#pragma mark - API requests

- (void)getAllPeopleWithSuccess:(void (^)(NSArray *people))successBlock andFailure:(void (^)(NSError *error))failureBlock;

//------------------------------------------------------------------------
#pragma mark - Fetch requests

- (NSArray*)fetchAllPeople;

@end
