//
//  APIHelper.m
//  TestAssignment
//
//  Created by Yury Pogrebnyak on 17.01.15.
//  Copyright (c) 2015 room8studio. All rights reserved.
//

#import "APIHelper.h"
#import "AppDelegate.h"
#import "Person.h"

#define API_BASE_URL @"http://json-generator.appspot.com/"
#define API_ENDPOINT_PEOPLE @"api/json/get/cbHNXtQQeq"

@implementation APIHelper

+ (APIHelper*)sharedInstance
{
    static APIHelper *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[APIHelper alloc] init];
    });
    return __sharedInstance;
}

- (void)saveContext
{
    [self.managedObjectContext saveToPersistentStore:nil];
}

//------------------------------------------------------------------------
#pragma mark - Initialization

- (id)init
{
    self = [super init];
    if (self) {
        // Setting up Core Data
        
        // turn off RestKit spam
//        RKLogConfigureByName("*", RKLogLevelError);
        
        // Enable Activity Indicator Spinner
        [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
        
        
        AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
        self.managedObjectModel = [appDelegate managedObjectModel];
        self.objectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:self.managedObjectModel];
        [self.objectStore createPersistentStoreCoordinator];
        
        NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"MillionAgents.sqlite"];
        NSError *error = nil;
        
        NSPersistentStore *store = [self.objectStore addSQLitePersistentStoreAtPath:storePath
                                                             fromSeedDatabaseAtPath:nil
                                                                  withConfiguration:nil
                                                                            options:nil
                                                                              error:&error];
        
        if (error) {
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            [fileManager removeItemAtPath:storePath error:&error];
            
            NSPersistentStore *store = [self.objectStore addSQLitePersistentStoreAtPath:storePath
                                                                 fromSeedDatabaseAtPath:nil
                                                                      withConfiguration:nil
                                                                                options:nil
                                                                                  error:&error];
            NSAssert(store, @"Failed to add persistent store with error: %@", error);
            
        }
        
        
        [self.objectStore createManagedObjectContexts];
        self.managedObjectContext = self.objectStore.mainQueueManagedObjectContext;
        
        self.objectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:self.objectStore.persistentStoreManagedObjectContext];
        
        // setup RestKit
        [self setupObjectManager];
    }
    return self;
}

//---------------------------------------------------------------------------------
#pragma mark - RestKit setup

- (void)setupObjectManager
{
    NSURL *baseURL = [NSURL URLWithString:API_BASE_URL];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    self.objectManager = [[RKObjectManager alloc] initWithHTTPClient:client];
    self.objectManager.managedObjectStore = self.objectStore;
    [RKObjectManager setSharedManager:self.objectManager];
    
    //---------
    // MAPPINGS
    //---------
    
    // People
    RKEntityMapping *peopleMapping = [RKEntityMapping mappingForEntityForName:@"Person"
                                                           inManagedObjectStore:self.objectStore];
    
    peopleMapping.identificationAttributes = @[@"guid"];
    [peopleMapping addAttributeMappingsFromDictionary:@{ @"guid" : @"guid",
                                                           @"company" : @"company",
                                                           @"email" : @"email",
                                                           @"address" : @"address",
                                                           @"phone" : @"phone",
                                                           @"balance" : @"balance" }];
    
    // Friends
    RKEntityMapping *friendsMapping = [RKEntityMapping mappingForEntityForName:@"Friend"
                                                           inManagedObjectStore:self.objectStore];
    
    [friendsMapping addAttributeMappingsFromDictionary:@{ @"id" : @"friendId",
                                                           @"name" : @"name" }];
    
    //--------------
    // RELATIONSHIPS
    //--------------
    
    [peopleMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"friends"
                                                                               toKeyPath:@"friends"
                                                                             withMapping:friendsMapping]];
    
    //---------------------
    // RESPONSE DESCRIPTORS
    //---------------------
    
    RKResponseDescriptor *peopleResponseDescriptor =
    [RKResponseDescriptor responseDescriptorWithMapping:peopleMapping
                                                 method:RKRequestMethodAny
                                            pathPattern:API_ENDPOINT_PEOPLE
                                                keyPath:nil
                                            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    [self.objectManager addResponseDescriptor:peopleResponseDescriptor];
}

//---------------------------------------------------------------------------------
#pragma mark - API requests

- (void) getAllPeopleWithSuccess:(void (^)(NSArray *))successBlock andFailure:(void (^)(NSError *))failureBlock
{
    [[RKObjectManager sharedManager] getObjectsAtPath:API_ENDPOINT_PEOPLE parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        NSArray* loadedPeople = mappingResult.array;
    
        if (loadedPeople) {
            // Save server order
            for (int i = 0; i < loadedPeople.count; i++) {
                Person* person = loadedPeople[i];
                person.sortOnServer = @(i);
            }
            
            NSArray *fetchedPeople = [self fetchAllPeople];
            // Clean old items if needed
            for (id person in fetchedPeople) {
                if (![loadedPeople containsObject:person]) {
                    [self.managedObjectContext deleteObject:person];
                }
            }
            
            [self.managedObjectContext saveToPersistentStore:nil];
            if (successBlock) {
                successBlock(loadedPeople);
            }
        }
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

//----------------------------------------------------------------------------------
#pragma mark - Fetch requests
- (NSArray*) fetchAllPeople
{
    NSFetchRequest* peopleFetchRequest = [[self.managedObjectModel fetchRequestTemplateForName:@"all_people"] copy];
    [peopleFetchRequest setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"sortOnServer" ascending:YES]]];
    NSArray* fetchedPeople = [self.managedObjectContext executeFetchRequest:peopleFetchRequest error:nil];
    return fetchedPeople ? fetchedPeople : [NSArray array];
}
@end
