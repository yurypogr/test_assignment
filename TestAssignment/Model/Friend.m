//
//  Friend.m
//  TestAssignment
//
//  Created by Yury Pogrebnyak on 17.01.15.
//  Copyright (c) 2015 room8studio. All rights reserved.
//

#import "Friend.h"
#import "Person.h"


@implementation Friend

@dynamic friendId;
@dynamic name;
@dynamic person;

@end
