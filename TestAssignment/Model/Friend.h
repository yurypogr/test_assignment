//
//  Friend.h
//  TestAssignment
//
//  Created by Yury Pogrebnyak on 17.01.15.
//  Copyright (c) 2015 room8studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Person;

@interface Friend : NSManagedObject

@property (nonatomic, retain) NSNumber * friendId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) Person *person;

@end
