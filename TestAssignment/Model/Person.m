//
//  Person.m
//  TestAssignment
//
//  Created by Yury Pogrebnyak on 17.01.15.
//  Copyright (c) 2015 room8studio. All rights reserved.
//

#import "Person.h"


@implementation Person

@dynamic guid;
@dynamic company;
@dynamic email;
@dynamic address;
@dynamic phone;
@dynamic balance;
@dynamic friends;
@dynamic sortOnServer;

@end
